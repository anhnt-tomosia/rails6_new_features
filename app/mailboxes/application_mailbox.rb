class ApplicationMailbox < ActionMailbox::Base
  # routing /something/i => :somewhere
  routing /^save@/i     => :forwards
end
