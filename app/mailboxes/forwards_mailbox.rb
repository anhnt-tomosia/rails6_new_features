class ForwardsMailbox < ApplicationMailbox
  def process
    #do awesome things here
    @forwarder ||= Person.where(email_address: mail.from)
  end
end
