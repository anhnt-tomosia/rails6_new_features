class Poodle < ApplicationRecord
  # connected to both the animals_primary db for writing and the animals_replica for reading
  connects_to database: { writing: :primary, reading: :animals_replica }
end
