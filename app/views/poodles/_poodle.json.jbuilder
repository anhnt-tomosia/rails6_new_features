json.extract! poodle, :id, :name, :weight, :height, :avatar, :birth, :created_at, :updated_at
json.url poodle_url(poodle, format: :json)
