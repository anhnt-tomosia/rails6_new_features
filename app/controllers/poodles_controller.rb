require_dependency 'cute_name'

class PoodlesController < ApplicationController
  before_action :set_poodle, only: [:show, :edit, :update, :destroy]

  # GET /poodles
  # GET /poodles.json
  def index
    @poodles = Poodle.all
  end

  # GET /poodles/1
  # GET /poodles/1.json
  def show
    Poodle.connected_to(role: :writing) do
      @poodle = Poodle.find(params[:id])
    end
  end

  # GET /poodles/new
  def new
    @poodle = Poodle.new(name: CuteName.generate)
  end

  # GET /poodles/1/edit
  def edit
  end

  # POST /poodles
  # POST /poodles.json
  def create
    @poodle = Poodle.new(poodle_params)

    respond_to do |format|
      saved = false
      Poodle.connected_to(role: :writing) do
        saved = @poodle.save
      end

      if saved
        format.html { redirect_to @poodle, notice: 'Poodle was successfully created.' }
        format.json { render :show, status: :created, location: @poodle }
      else
        format.html { render :new }
        format.json { render json: @poodle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /poodles/1
  # PATCH/PUT /poodles/1.json
  def update
    respond_to do |format|
      if @poodle.update(poodle_params)
        format.html { redirect_to @poodle, notice: 'Poodle was successfully updated.' }
        format.json { render :show, status: :ok, location: @poodle }
      else
        format.html { render :edit }
        format.json { render json: @poodle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /poodles/1
  # DELETE /poodles/1.json
  def destroy
    @poodle.destroy
    respond_to do |format|
      format.html { redirect_to poodles_url, notice: 'Poodle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poodle
      @poodle = Poodle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def poodle_params
      params.require(:poodle).permit(:name, :weight, :height, :avatar, :birth)
    end
end
