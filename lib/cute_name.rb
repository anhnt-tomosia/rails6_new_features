class CuteName
  def self.generate
    [
      :Abby,
      :Allie,
      :Annie,
      :Angel,
      :Apple,
      :Bailey,
      :BammBamm,
      :Bella
    ].sample
  end
end
