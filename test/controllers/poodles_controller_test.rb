require 'test_helper'

class PoodlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @poodle = poodles(:one)
  end

  test "should get index" do
    get poodles_url
    assert_response :success
  end

  test "should get new" do
    get new_poodle_url
    assert_response :success
  end

  test "should create poodle" do
    assert_difference('Poodle.count') do
      post poodles_url, params: { poodle: { avatar: @poodle.avatar, birth: @poodle.birth, height: @poodle.height, name: @poodle.name, weight: @poodle.weight } }
    end

    assert_redirected_to poodle_url(Poodle.last)
  end

  test "should show poodle" do
    get poodle_url(@poodle)
    assert_response :success
  end

  test "should get edit" do
    get edit_poodle_url(@poodle)
    assert_response :success
  end

  test "should update poodle" do
    patch poodle_url(@poodle), params: { poodle: { avatar: @poodle.avatar, birth: @poodle.birth, height: @poodle.height, name: @poodle.name, weight: @poodle.weight } }
    assert_redirected_to poodle_url(@poodle)
  end

  test "should destroy poodle" do
    assert_difference('Poodle.count', -1) do
      delete poodle_url(@poodle)
    end

    assert_redirected_to poodles_url
  end
end
