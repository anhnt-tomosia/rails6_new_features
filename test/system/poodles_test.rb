require "application_system_test_case"

class PoodlesTest < ApplicationSystemTestCase
  setup do
    @poodle = poodles(:one)
  end

  test "visiting the index" do
    visit poodles_url
    assert_selector "h1", text: "Poodles"
  end

  test "creating a Poodle" do
    visit poodles_url
    click_on "New Poodle"

    fill_in "Avatar", with: @poodle.avatar
    fill_in "Birth", with: @poodle.birth
    fill_in "Height", with: @poodle.height
    fill_in "Name", with: @poodle.name
    fill_in "Weight", with: @poodle.weight
    click_on "Create Poodle"

    assert_text "Poodle was successfully created"
    click_on "Back"
  end

  test "updating a Poodle" do
    visit poodles_url
    click_on "Edit", match: :first

    fill_in "Avatar", with: @poodle.avatar
    fill_in "Birth", with: @poodle.birth
    fill_in "Height", with: @poodle.height
    fill_in "Name", with: @poodle.name
    fill_in "Weight", with: @poodle.weight
    click_on "Update Poodle"

    assert_text "Poodle was successfully updated"
    click_on "Back"
  end

  test "destroying a Poodle" do
    visit poodles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Poodle was successfully destroyed"
  end
end
