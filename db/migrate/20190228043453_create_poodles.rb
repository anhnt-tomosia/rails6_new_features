class CreatePoodles < ActiveRecord::Migration[6.0]
  def change
    create_table :poodles do |t|
      t.string :name
      t.integer :weight
      t.integer :height
      t.string :avatar
      t.datetime :birth

      t.timestamps
    end
  end
end
